package com.companies.companiesapi.service;

import com.companies.companiesapi.model.Company;
import com.companies.companiesapi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    public void updateCompany(Company c) {
        Assert.notNull(c, "The company not found.");
        Assert.isTrue(c.getName() != null, "Company name not specified!" );
        Assert.isTrue(c.getLogo() != null, "Company logo not specified!");
        Assert.isTrue(c.getEmployees() > 0, "Company must have at least 1 employee!");
        Assert.isTrue(c.getName().length() > 2, "Company name should be longer than 2 chars.");

        if (c.getId() > 0) {
            //muutmine
            companyRepository.updateCompany(c);
        } else {
            companyRepository.addCompany(c);
        }
    }

    public void removeCompany(@PathVariable("id") int id) {
        Assert.isTrue(id > 0, "Company must have id!");
        Assert.notNull(companyRepository.fetchCompany(id), "The company was not found!");
        companyRepository.removeCompany(id);
    }
}
